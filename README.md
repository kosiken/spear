# Spear Hobby app

a [Sails v1](https://sailsjs.com) application


### Links

+ [Sails framework documentation](https://sailsjs.com/get-started)
+ [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
+ [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
+ [Community support options](https://sailsjs.com/support)

* [Spears application](https://spears.herokuapp.com)
* [Postman collection](https://spears.herokuapp.com/collection)


### Version info

This app was originally generated on Wed Jan 23 2019 20:09:11 GMT+0100 (West Africa Standard Time) using Sails v1.1.0.
it is currently in version 26 please note that this application is a test application.



## Authors

* **ALLISON KOSY** - *Initial work* - [Spear](https://github.com/kosiken)






