

//import $ from 'jquery'

$('#home-form').on('submit',function(event){
    event.preventDefault();
    const {err, message} = isValid()
  return  err ?  alert(message) : fetches('register','/login')

    
})

function vals(params) {
  return $(params).val()  
}

function isValid (){
    const validObj = Object.create(null)
 validObj.password =  $('#password').val()===$('#confirmPassword').val() 
 validObj.email = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(vals('#email'))
 validObj.phonenumber = /^\d{10}$/.test(vals('#phonenumber'))
let message = ''
 if (!validObj.password) return {err: true, message: message += ' Passwords don\'t match'}
 if (!validObj.email) return {err:true, message: message += ' Email is not valid'}
 if(!validObj.phonenumber) return {err:true, message: message+=' Enter a valid Phonenumber' }
 return {err: false, message:null}
}

function fetches (key){
    $('button').attr('disabled',true)
    const data = {
        username: vals('#username')||'',
        email: vals('#email') ||'',
        phonenumber:   key !== '/login'? vals('#countryCode').match(/\+\d+/)[0] + vals('#phonenumber') || '':'',
        password: vals('#password')
    }
   // console.log(JSON.stringify(data))
       fetch(key, {
       method:'POST',
       body:JSON.stringify(data)
   }).then(resp => {
     //  console.log(resp.json())
  return  resp.json()}).then(obj=>{
       console.log(obj)
       
       if(!obj.success){ 
         dispError(obj)
        }
        else if (obj.success){
            const ele =document.querySelector('#home-form')

            ele.innerHTML =`<a href="/login" class="button">Confirm</a>`
        }
        if(obj.hasOwnProperty('user')){
           return obj.user? dispConfirm('dashboard/' + obj.user.username): dispError(obj,'#login-form')
        }
   })
}

$('#login-form').on('submit', async  function (e){
    e.preventDefault()
     fetches('/login','/dashboard')
    
})
function dispError(obj={message: 'no message'},type = '#home-form'){
const ele = $(`<div class="err ">${obj.message}</div>`)
           
$(type).prepend(ele)
setTimeout(function(){
$('.err').remove()
$('button').attr('disabled',false)
},1500)
}


function dispConfirm(params) {
    const ele =document.querySelector('#login-form')

    ele.innerHTML =`<a href="${params}" class="button">Confirm</a>`
}
$('#hobby-form').on('submit',async(e)=>{
    e.preventDefault()
  const data= {name: vals('#name')||'',
    category: vals('#cat') ||'',
    descrip: vals('#descrip')
   }
 const response = await fetch('/hobies',{
     method:'POST',
     body: JSON.stringify(data)
 })
 const unicorn = await response.json()
 console.log(unicorn)

 if(unicorn.success) getPpl(unicorn)
 else dispError(unicorn,'#login-form')
 
})

 function getPpl(obj){
  
$('#hobby-form').append($('<p class="response">' + obj.message + '</p>'))

$('#hobby-form').append($(obj.mail.html))

 console.log(obj)
}