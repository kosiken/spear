
// fallback for heroku environment variables
module.exports =
 {
     password: process.env.SP_PASSWORD,
     email: process.env.SP_EMAIL,
     ACCSID: process.env.ACC_SID ,
     TOKEN: process.env.ACC_TOKEN ,
     phoneNumber:process.env.SP_NUMBER,
     clientId: process.env.CLI_ID,
     clientSecret: process.env.CLI_SEC,
     refToken: process.env.REF_TOK
 }