/* eslint-disable no-useless-escape */

/**
 * Object to lessen the code length of the Hobby Controller 
 * Exports helper functions because using the helper
 *  functions folder kept throwing an error
 *
 */


module.exports  = {
    validation: function(sender='null', reciever='null') {
        if (!(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(sender) )){
            return {
                success: false,
                message: 'INTERNAL SERVER ERROR, YOUR REQUEST COULD NOT BE PROCESSED',
                code: 500
            }
        }
        if (!(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/).test(reciever)) { 
            return {
                success: false,
                message: reciever + ' IS NOT A VALID EMAIL ADDRESS',
                code: 417
            }
    
        }
        return {
            success:true,
            message: 'YOU HAVE ENTERED THE RIGHT EMAIL',
            code:304
        }
    
    },

    template: function(sender = ' ' ,reciever= 'allisonkosy@gmail.com', message = 'COULD NOT DELIVER TO A CLIENT BECAUSE OF SERVER ERROR', 
        subject ='SPEARS ERROR')
    {
        return{
            from:sender ,
            to: reciever,
            subject: subject,
            html:`<p>${message}</p><br><br> Spears &trade.`
        }
    }
}

