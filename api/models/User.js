/* eslint-disable no-undef */
/* eslint-disable no-console*/



/**
 * User.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */



module.exports = {

    schema: true,

    attributes: {
        email: {
            type: 'string',
            isEmail: true,
            required: true,
            unique: true
        },
        username: {
            type: 'string',
            required: true,
            unique: true
        },
        phone: {
            type: 'string',
            unique: true,
            required: true
        },
        password: {
            type: 'string',
            required: true
        },
        hobbies: {
            collection: 'hobby',
            via: 'user'
        }
    },

    customToJSON: function() {
        return _.omit(this, ['password'])
    },
    datastore: 'mongodb'

}



