/* eslint-disable no-undef */
/* eslint-disable no-console*/
/**
 * HobbyController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const nodemailer = require('nodemailer')

const {email, ACCSID, phoneNumber, TOKEN, clientId, clientSecret, refToken} = require('../mssgs/password'),
    client = require('twilio')(ACCSID, TOKEN)











const {validation, template} = require('../mssgs/email')


module.exports = {

    // route handler for add requests
    addHobby: (req, res) => {
        let statusMessage = 'DEAR'
        let success, code

        statusMessage += ` ${req.user.email}`
        // validating if the email is correct
        const obj = validation('kosikenspears@gmail.com', req.user.email)

        success = obj.success; code = obj.success ; statusMessage += ` ${obj.message} `

        if (!success) {
            res .json({
                success: success,
                code: code,
                message: statusMessage
            })
        }
        else{
            let mail
            //  console.log(req.session.id)
            Hobby.findOrCreate({
                name: req.body.name,
                user:req.session.passport.user
        
            }, {
                name: req.body.name,
                description: req.body.descrip,
                category: req.body.category,
                user:req.session.passport.user
      
            }, (err, existingHobby, wasCreated) => {
           
                if (err) {
                    success = false
                    code = 400
                    statusMessage = 'Internal error, this is an internal server error send an email to admin ' +email
                    mail = template(email, undefined,  'COULD NOT DELIVER TO A CLIENT '  + req.user.email + ' BECAUSE OF SERVER ERROR' )
                    res.status(500).send({
                        success: success,
                        code: code,
                        message: statusMessage,
                        m
                    })
                }
                if (existingHobby&&!wasCreated) {
                    success = false
                    code = 400
                    statusMessage += 'You\'ve already registered this hobby ' + req.body.name
                    res.status(code).send({
                        success: success,
                        code: code,
                        message: statusMessage,
                        
                    })
                } 
                //  console.log(newUser,existingHobby,err)
                if (wasCreated) {
                
                    success = true
                    code = 201
                    statusMessage += ' Hobby Created '
                    mail = template(email, req.user.email, statusMessage + 'Congratulations', 'SPEARS MESSAGE')
                    // will throw an error with an unfriendly firewall
                    
                    const transporter = nodemailer.createTransport({
                        service:'gmail',
                        auth: {
                            type:'OAuth2',
                            user:email,
                            clientId:clientId,
                            clientSecret:clientSecret,
                            refreshToken:refToken
                            
                        }
                    })
                    transporter.sendMail( mail, function (err, info) {
                        if (err) console.log(err)
                        else console.log(info)
                        transporter.close()


                        client.messages.create({
                            body: `${req.body.name} was added from Spears`,
                            from: phoneNumber,
                            to: req.user.phone
                        },(err, item)=>{
                            if(err){
                                console.log(err)
                                res.send({success:false})
                            }

                            else  { res.status(code).send({
                                success: success,
                                code: code,
                                message: statusMessage,
                                mail: mail
    

                            })
                            console.info(item.sid)
                            }
                           
                                    
                        })
                                 
                    } )
                  
                }
            })
        }
    },
    fake: (req,res)=>{
        res.sendFile(__dirname+'/categories.json')
    },
    dispHobbies:  (req,res)=>{
        // edit front end on bitbucket
        Hobby.find({owner:req.session.passport.user},(err,hobbies)=>{
            let success = false, message='INTERNAL ERROR',con
            if( err) {
                con = []
            }
            if (hobbies) {
                con=hobbies
            }
            res.send({success: success, message:message, payload:con})
        })
    },
    user: (req,res)=>{       
        res.send(req.user)
    },
    
     postman: (req, res) => {
        Hobby.find({}, function (err, hobbies) {
            if(err) {
                console.log (err)
                res.status(500).send(
                {
                    success:false,
                    message: err,
                    payload:[]
                })
            }
            else {
                res.status(200).send(
                {
                    success:true,
                    message:'KOSY\'S POSTMAN COLLECTION',
                    payload: hobbies 
                })
            }
            // body...
        })
    }





}


