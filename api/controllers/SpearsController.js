/**
 * SpearsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
 
    spear:function (req, res) {
        if(!req.user){ res.redirect('../login')
        }
        else  {
            res.view('pages/dashboard')
        }
    }
}

