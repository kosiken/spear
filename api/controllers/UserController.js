/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var statusMessage;
const bcrypt = require('bcryptjs')
module.exports = {

  createUser: function (req, res) {
  //  console.log(req.body, req.data)
    User.findOrCreate({
      username: req.body.username,
      email: req.body.email,
    }, {
      username: req.body.username,
      email: req.body.email,
      phone: req.body.phonenumber,
      password: hasher(req.body.password)
    }, (err, existingUser, newUser) => {
      let success, code
      if (err) {
        console.log(err)
        success = false;
        code = 500
        statusMessage = 'ERROR ADDING USER, CHECK YOUR DETAILS';
      }
      if (existingUser) {
        success = false
        code = 400
        statusMessage = 'USER ALREADY EXISTS';
      } 
      console.log(newUser,existingUser,err)
      if (newUser) {
        success = true
        code = 304
        statusMessage = 'USER SUCCESSFULLY CREATED';
      }
      
      res.send({
        success: success,
        code: code,
        message: statusMessage
      })
    });

  }

};
function hasher(params) {
  if(!params[0]) return "invalid"
  let salt = bcrypt.genSaltSync(10)
  let passwordHash = bcrypt.hashSync(params,salt)
  return passwordHash
}