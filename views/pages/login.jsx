/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
import React, {Component} from 'react'
import {Head} from './src/components'


export default class Login extends Component {
    constructor(props) {
        super(props)
  
        this.state = {
            username: '',
            password: '',
            statusMessage: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

    }
  
    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0
    }
  
    handleChange(event) {
        this.setState({
            [event.target.id]: event.target.value
        })
    }
  
    handleSubmit(event) {
        console.log(this.state)
      
    }
    
    render() {
        return (
            <html>
                <Head/>
                <body className="homeBody">

                    <div className="content">
    
                        <div className="logo-place">

                            <img src="/images/logo.png" alt="" srcSet=""/>
                            <h1 className="h1 bold">Spears</h1>
                            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Inventore, sit in illo sunt nam deserunt doloremque quae nihil non dicta reiciendis quod voluptatibus distinctio adipisci eaque atque cupiditate alias repellendus?</p></div> 
    

                        <div className="form-reg-div">	
                            {this.state.username}
                            <form action="" method="POST" onSubmit={this.handleSubmit} className="form-reg"  id="login-form">	
                                <div className="group">
                                    <input type="text" name="username" onChange={this.handleChange} value={this.state.username} id="username" className="group-bottom" placeholder="holder"/>	
        
        
                                </div>	
               
           
                                <div className="group">	
                                    <input type="password" name="password" id="password"  onChange={this.handleChange}  className="group-bottom" placeholder="Password"/>	
            
        
                                </div>	
            

                                <div className="subs">
                                    <button type="submit">Login</button>
                                </div>
                            </form>
        
                            <script src="dependencies/jquery.min.js"></script>
                            <script src="/scripts/ajax.js"></script>      
                        </div>


                    </div>


                </body>
            </html>
        ) 
    }
}