import React, {Component}  from 'react'
// import ReactDom from 'react-dom'
const components = {
    Head: class Head extends Component {
        constructor(props){
            super(props)
        }
        render() {
            return (
                <head>
                    <meta charSet="utf-8" />
                    <meta httpEquiv="X-UA-Compatible" content="IE=edge"></meta>
                    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
                    <title>Home|Spears</title>
                    <link rel="stylesheet" href="/dependencies/skeleton.css"/>
                    <link rel="stylesheet" href="/styles/main.css"/>
                    
                    
                  
                    <script src="/dependencies/sails.io.js"></script>
                </head>
            )
        }

    },
    Form: class Form extends Component{
        constructor(props) {
            super(props)
        
            this.state = {
                email: '',
                password: '',
                passwordCon: '',
                countryCode: '',
                username:'',
                phonenumber:''
            }
            this.handleChange=  this.handleChange.bind(this)

        }
        
        handleChange (event)  {
            console.log(event,'kk', this)
            this.setState({
                [event.target.id]: event.target.value
            })

         
        }

        render() {
            return (
                <div className="form-reg-div">	
                    {this.state.username}
                    <form  action="" method="POST" className="form-reg"  id="home-form">	
                        <div className="group">
                            <input type="text" name="username" onChange={this.handleChange} value={this.state.countryCode} id="username" className="group-bottom" placeholder="holder"/>	
        
        
                        </div>	
                        <div className="group">	
                            <input type="email" name="email" id="email"  onChange={this.handleChange}  className="group-bottom" placeholder="Email"/>	
            
        
                        </div>	
                        <div className="group">	
                            <select name="countryCode" id="countryCode">
                                <option value="Enter your country code +234" dataid= "00000">Enter your country code +234</option>
                            </select>
                            <input type="text" name="phonenumber" id="phonenumber"  onChange={this.handleChange}  className="group-bottom" placeholder="Phone Number"/>	
            
        
                        </div>	
                        <div className="group">	
                            <input type="password" name="password" id="password"  onChange={this.handleChange}  className="group-bottom" placeholder="Password"/>	
            
        
                        </div>	
                        <div className="group">	
                            <input type="password" name="confirmPassword" id="confirmPassword"  onChange={this.handleChange}  className="group-bottom" placeholder="Confirm Password"/>	
            
        
                        </div>	

                        <div className="subs">
                            <button type="submit">Register</button>
                        </div>
                    </form>
                    <script src="/dependencies/jquery.min.js"></script>
                    <script src="/scripts/ajax.js"></script>
                </div>
            )
        }
    },

    SearchBar: class SearchBar extends Component{
        constructor(props) {
            super(props)
            this.handleChange = this.handleChange.bind(this)
            this.state = {
                placeholder: 'Search'

            }
        
        }
        componentDidMount(){
            this.handleChange = this.handleChange.bind(this)
            console.log('kk') 
        }

        handleChange(e) {
            e.preventDefault()
            this.setState({
                placeholder: 'ggg'
            })
        }
          
        render() {
            return(
                <div className="searches">
                    <form  className="searchcue" >
                        <div className="group">
                            {this.state.placeholder} 
                            <input type="search" name="search" onChange={this.handleChange} id="key" placeholder={this.state.placeholder}/>
                  
                        </div>
                  
                    </form>
                  
                </div>
            )
        }
    },

    HobbyList: class HobbyList extends Component{
        constructor(props){
            super(props)
            this.handleChange=this.handleChange.bind(this)
        }
        handleChange (event)  {
            console.log(event,'kk', this)
        }
        render() {
            return   ( 
                <div className="hobby-div">
                    <form action="" id="hobby-form">
                        <h3>Register Hobby</h3>
                        <div className="group">
                            <label htmlFor="name"><strong>Name</strong></label>
                            <input type="text" name="name" id="name" placeholder='name'/>
                        </div>
                        <div className="group">
                            <label htmlFor="category"><strong>Category</strong></label>
                            <select name="category" id="cat" placeholder="category">
                            </select>
                        </div>
                        <div className="group">
                            <label htmlFor="descrip"><strong>Description</strong></label>
                            <textarea name="descrip" id="descrip" value="jj"onChange={this.handleChange} cols="30" rows="10"/>
                        </div>
                        <button type="submit">Submit</button>
                    </form>
                    <div className="dblist">
                        <ul className="hobbyli" id="liter"></ul>
                    </div>
                    <script src="/dependencies/jquery.min.js"></script>
                    <script src="/scripts/ajax.js"></script>
                    <script src="/scripts/dashboard.js"></script>

                </div>
            )
        }

    }

    
}

module.exports= components//